const display = document.querySelector('.display input');
const ecuals = document.querySelector('input[value = "="]');
const zero = document.querySelector('input[value = "0"]');
let memory = 0, memoryShow = false, operand, operand2value = '', operator;

function getNum(key) {
    if (!isNaN(key) || key === '.') {
        if (operand2value.includes('.') && key === '.' || operand2value === zero && key === zero) {
            return;
        } else if (operand2value === zero && !isNaN(key)) {
            operand2value = key
            display.value = operand2value;
        } else if (!operand2value && key === '.') {
            operand2value = zero + key;
            display.value = operand2value;
        } else {
            operand2value += key;
            display.value = operand2value;
        }
    }
}

function calculator(e) {
    let key;   
    if (e.type === 'click') {
        key = e.target.value;
    } else if (e.type === 'keydown' && e.key) {
        key = e.key;
    }  
    if (!key) return;  
    if (key !== 'mrc') memoryShow = false;
    if (operand && operator) ecuals.disabled = false;   
    if (display.value) memoryUse(key);
    if (display.value && key.match(/[-+/*]/)) {
        if (!operand) {
            operator = key;
            operand = display.value;
            operand2value = '';
        } else if (operand) {
            calc(operand, operator, operand2value);
            operator = key;
            operand = display.value;
            operand2value = '';
        }
    } else if (operand && operator && (key === '=')) {
        calc(operand, operator, operand2value);
        operand = '';
        operand2value = '';
    }
    getNum(key);
    clear(key);
}

function calc(first, operator, second) {
    ecuals.disabled = true;
    switch (operator) {
        case '+':
            display.value = +first + +second;
            break;
        case '-':
            display.value = first - second;
            break;
        case '*':
            display.value = first * second;
            break;
        case '/':           
            display.value = first / second;            
            break;
        default:
            break;
    }
}

function clear(key) {
    if (key === 'C') {
        display.value = '';
        operand2value = '';
        operand = '';
    }
	if (key === 'c') {
        display.value = '';
        operand2value = '';
        operand = '';
    }
	if (key === 'с') {
        display.value = '';
        operand2value = '';
        operand = '';
    }
	if (key === 'С') {
        display.value = '';
        operand2value = '';
        operand = '';
    }
}

function showMemorySign() {
    display.insertAdjacentHTML('beforebegin', `<span class="memory-pic">M</span>`);
}

function memoryUse(key) {
    if (key === 'm+' && display.value) {
        memory += +display.value;
        showMemorySign();
    } else if (key === 'm-') {
        memory -= +display.value;
        showMemorySign();
    } else if (key === 'mrc' && memoryShow && document.querySelector('.memory-sign')) {
        memory = 0;
        document.querySelector('.memory-sign').remove();
    } else if (key === 'mrc') {
        display.value = memory;
        operand2value = memory;
        memoryShow = true;
    }
}

document.body.addEventListener('click', calculator);
document.body.addEventListener('keydown', calculator);
